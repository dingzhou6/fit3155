import sys

"""

name         : Ding Zhou Chia
student_ID   : 27031330

"""
class Node:
    def __init__(self):
        self.suffixIndex = None
        self.start = None
        self.end = None
        self.suffixLink = None
        self.edges = {}

class suffixTree:

    def __init__(self,words):

        self.words = words
        self.size = len(self.words)
        self.remainingCount = 0
        self.splitCurrentEnd = None
        self.leafEnd = 0

        # which is insert into node while dont know whats the end gonna be
        self.max = self.size + 1
        self.previousNode = None
        self.activeEdge = -1
        self.activeLength = 0
        # set root node
        self.root = Node()
        self.root.start = -1
        self.root.end = -1
        # initial activenode as root node
        self.activeNode = self.root
        self.build_suffix_tree()


    def edge_length(selfself,node):
        return node.end - node.start +1

    def build_suffix_tree(self):

        for i in range(self.size):
            self.extendtree(i)
            # rule 1
            # each round update leafend
            self.leafEnd = i
        # set suffixIndex
        self.set_SuffixIndex(self.root, 0)


    def set_SuffixIndex(self, node, height):

        # dfs to get each node
        # set if node dont have child then set suffixIndex and update it end
        for x in node.edges :
            if len(node.edges[x].edges) !=0 :
                self.set_SuffixIndex(node.edges[x], height + self.edge_length(node.edges[x]))
            else:
                node.edges[x].suffixIndex = self.size - (self.leafEnd - node.edges[x].start +1) - height
                # update each end
                node.edges[x].end = self.leafEnd


    def get_SuffixIndex(self, node, alist):

        # dfs to get each node
        # if node dont have any child , get the index
        for x in node.edges :
            if len(node.edges[x].edges) !=0 :
                self.get_SuffixIndex(node.edges[x], alist)
            else:
                alist.append(node.edges[x].suffixIndex)

        return alist

    def split_node(self,Curr_node,j):

        self.splitCurrentEnd = Curr_node.start + self.activeLength - 1

        # New internal node
        splitNode =  Node()
        splitNode.start = Curr_node.start
        splitNode.end = self.splitCurrentEnd
        splitNode.suffixLink = self.root
        splitNode.suffixIndex = -1

        # replace current with new splitNode
        self.activeNode.edges[self.words[self.activeEdge]] = splitNode
        # New leaf coming out of new internal node
        splitNode.edges[self.words[j]] = Node()
        splitNode.edges[self.words[j]].start = j
        splitNode.edges[self.words[j]].end = self.max
        splitNode.edges[self.words[j]].suffixLink = self.root
        splitNode.edges[self.words[j]].suffixIndex = -1

        Curr_node.start += self.activeLength
        # store the current node into splitNode
        splitNode.edges[self.words[Curr_node.start]] = Curr_node
        return splitNode


    def extendtree(self, j):


        # increase remaining count each phase
        self.remainingCount += 1
        self.previousNode = None
        while (self.remainingCount > 0) :

            # if activelength is 0 only set active edges as current j position
            # if not means current prefix exist in the tree
            if (self.activeLength < 1):
                self.activeEdge = j

            # rule 2
            # if not exist in current node edges
            # extend the node
            if (self.words[self.activeEdge] not in self.activeNode.edges):

                # suffix link
                # if split a new node will have new suffix link
                # link the new node to the root
                if (self.previousNode is not None):
                    self.previousNode.suffixLink =self.activeNode
                    self.previousNode = None

                # add current char into edges
                newNode = Node()
                self.activeNode.edges[self.words[self.activeEdge]] = newNode
                newNode.start = j
                newNode.end = self.max
                newNode.suffixLink = self.root
                newNode.suffixIndex = -1


            else:

                #  if found active edge , set current node
                Curr_node = self.activeNode.edges[self.words[self.activeEdge]]

                #  if active length >= current node edge length
                #  current active node jump to the 'next' node
                current_length = self.edge_length(Curr_node)
                if (self.activeLength >= current_length):
                    self.activeLength -= current_length
                    self.activeEdge += current_length
                    self.activeNode = Curr_node
                    continue

                # Rule 3
                # if prefix found in exist edges , do nothing
                if (self.words[j] == self.words[Curr_node.start + self.activeLength]):

                    # link previousnode to current active node
                    if (self.previousNode is not None):
                        self.previousNode.suffixLink = self.activeNode

                    #  if found in current edges set 'previous' node to root
                    self.previousNode = self.activeNode
                    self.activeLength += 1
                    break

                # split the edges
                splitNode = self.split_node(Curr_node,j)

                if (self.previousNode is not None):

                    # link previous node to the new splitnode
                    # create suffix link
                    self.previousNode.suffixLink = splitNode

                # if no previousnode found then set previousnode as new splitnode
                # or set previousnode after link the previousnode with new splitnode
                self.previousNode = splitNode

            self.remainingCount -= 1
            # follow the suffix link

            # suffix link
            # jump to next node by using suffix link
            if (self.activeNode != self.root):
                self.activeNode = self.activeNode.suffixLink

            elif ((self.activeLength >= 1) and (self.activeNode == self.root)  ):
                self.activeLength -= 1
                # move to next prefix position
                self.activeEdge = j - self.remainingCount + 1


def bwt(alist,txt):
    """

    :param alist:  the suffix list


    :return: return the suffix array that sorted
    complexity : O(n)>>ukkonen tree  + o(mn)>>(radix sort)

    """

    # sort first character and return a new list
    newList = radix_sort(alist, 2 ** 0 - 1)

    bwt_string =""

    # get suffix array sindex
    for i in newList:
        bwt_string += txt[i[0]-1]

    return bwt_string

def radix_sort(alist, i):
    """
    radix sort , sort first character with 2**0 -1
    :param alist:
    :param i:
    :return: reuturn the sorted list
    complexity : O(mn)
    """

    num = len(alist)
    newList = [[] for i in range(num)]
    bucket = [[] for i in range(127)]

    for k in range(len(alist)):
        word = alist[k][1]
        value = ord(word[i])
        bucket[value].append(alist[k])

    a = 0
    rank = 0
    for k in range(127):
        buck = bucket[k]
        if len(buck) >1 :
            # if one bucket got more than 1 element
            # sort again
            r_list = radix_sort(buck,i+1)

            for j in r_list:
                sindex = j[0]
                word = j[2]
                newList[a] = [sindex, rank, word]
                rank +=1
                a += 1

        else:
            for j in buck:
                sindex = j[0]
                word = j[1]
                newList[a] = [sindex, rank, word]
                a += 1
                rank+=1

    return newList


def read_file(infile):
    """
    read the content that in the file
    complexity : O(n)
    return string
    """

    infile = open(infile, "r")

    for item in infile:
        item = item.strip("\n")


    infile.close()
    return item

def write_file(bwt_string):

    """
    :param filename: filename that want to write
    :return: to create or open a file

    """

    infile = open("outputbwt.txt", "w")
    infile.write(bwt_string)
    infile.close()


if __name__ == "__main__":
    text = read_file(sys.argv[1])

    Tree = suffixTree(text)

    suffixindex =[]
    Tree.get_SuffixIndex(Tree.root, suffixindex)

    suffix_list = []
    for i in suffixindex:
        suffix_list.append([i,text[i:]])

    # for testing purpose

    # suffix_list.sort()
    # for i in suffix_list:
    #     print(i)


    write_file(bwt(suffix_list,text))

