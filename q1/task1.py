import sys

"""

name         : Ding Zhou Chia
student_ID   : 27031330

"""


def readfile(infile):

    string = ''
    infile = open(infile, "r")
    for item in infile:
        string = item.strip("\n")


    infile.close()
    return string



def write_file(hamming,position):

    """
    :param filename: filename that want to write
    :return: to create or open a file
    time complexity : o(N)
    """

    infile = open("output_hammingdist.txt", "w")
    for i in range(len(position)):
        w = str(position[i]+1) + "\t" + str(hamming[i]) + "\n"
        infile.write(w)

    infile.close()


def body(pattern,txt):
    z1 = compare(txt,pattern)
    z2= compare(txt[::-1],pattern[::-1])
    z2=z2[::-1]
    position  = []
    hamming = []
    for i in range(len(z1)):

        # if match string
        if z1[i] == len(pattern):
            position.append(i - len(pattern) - 1)
            hamming.append(len(pattern) - z1[i])

        elif (z1[i] + z2[i-2] == len(pattern) - 1 and i -1<= len(txt)):
            position.append(i - len(pattern) - 1)
            hamming.append(len(pattern) - (z1[i] + z2[i - 2]))


    write_file(hamming,position)

# z box algorithm

def compare(txt,pattern):
    """
    time complexity : O(nm) - linear time
    :param txt:  text
    :param pattern: pattern
    :return: zbox
    """
    # combine pattern and string together
    long_string = pattern + "$" + txt

    z = [0] * len(long_string)
    left = 0
    right = 0

    for k in range (1,len(long_string)):

        if(k > right ):

            left = right = k
            while(right <len(long_string) and long_string[right] == long_string[right - left]):

                right += 1
            z[k] = right - left
            right -= 1

        else:
            # if not out of z box
            if z[k - left] < right - k + 1:
                z[k]=z[k - left]

            else :
                # recomparing
                i = right + 1
                while (i < len(long_string) and long_string[i] == long_string[i - k]):
                    i += 1

                z[k] = i - k
                left = k
                right = i -1

    return z

txt ="bbcaefadcabcpqr"
pattern = "abc"

pattern = readfile(sys.argv[2])
txt = readfile(sys.argv[1])

body(pattern,txt)