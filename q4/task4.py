import sys
"""

name         : Ding Zhou Chia
student_ID   : 27031330

"""


def readfile(infile):

    graph= []

    infile = open(infile, "r")

    for item in infile:
        list = item.split(" ")
        tmp =[]

        # convert str into int
        # save into list
        for i in list :
            a = i.strip()
            a = int(a)
            tmp.append(a)
        graph.append(tmp)

    infile.close()
    return graph



def write_file(mst):

    """
    :param filename: filename that want to write
    :return: to create or open a file
    time complexity : o(N)
    """

    infile = open("output_kruskal.txt", "w")
    for i in range(len(mst)):
        edges = mst[i]
        w  = str(edges[0]) + " " + str(edges[1])  + " " +  str(edges[2]) + "\n"
        infile.write(w)


    infile.close()

# quick sort
# time complexity O(n log n)
def quicksort(alist):

    quicksort_aux(alist,0,len(alist)-1)

def quicksort_aux(alist,start,end):
    if start < end:
        boundary = partition(alist,start,end)
        quicksort_aux(alist,start,boundary-1)
        quicksort_aux(alist,boundary+1,end)

def partition(alist,start,end):

    # choose last element as pivot
    pivot = alist[end][-1]
    swap(alist,start,end)
    index = start

    for k in range(start +1,end+1):
        if alist[k][-1] < pivot :
            index +=1
            swap(alist,k,index)

    swap(alist,start,index)
    return index

def swap(alist,i,j):
    alist[i],alist[j] = alist[j],alist[i]


def kruskal(alist,vertice):

    # set all vertice parents as -1
    for v in vertice:
        make_disjoint_set(v)

    # sort list
    quicksort(alist)

    mst = []
    for edges in alist :
        v,u ,weight = edges
        if find(v) != find(u):
            union(v,u)
            mst.append(edges)

    quicksort(mst)
    return mst


def union(a,b):
    # union by rank
    root_a = find(a)
    root_b = find(b)
    if (root_a == root_b):
        return

    height_a = -parent[root_a]
    height_b = -parent[root_b]

    if height_a > height_b :
        parent[root_b] = root_a


    elif  height_b > height_a:
        parent[root_a] = root_b

    else:
        parent[root_a] = root_b
        parent[root_b] = -(height_b + 1)


def make_disjoint_set(v):
    parent[v] = -1


def find(a):
    # find using path compression
    if parent[a] < 0 :
        return a

    else:
        parent[a] = find(parent[a])
        return parent[a]


graph = readfile(sys.argv[1])
vertice= []
number = 0

#  get vertices into vertices list
#  if not in vertices list append into it
for i in range (len(graph)):

    if graph[i][0] not in vertice:
        # get biggest node number
        if graph[i][0] > number :
            number = graph[i][0]

        vertice.append(graph[i][0])

    if graph[i][1] not in vertice:
        # get biggest node number
        if graph[i][1] > number :
            number = graph[i][1]

        vertice.append(graph[i][1])

# assume node is always start from 1
number+=1
parent = ['']* number
mst = (kruskal(graph,vertice))
write_file(mst)
print(parent)
